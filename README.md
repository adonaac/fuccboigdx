[**fuccboiGDX**](http://fuccboi.moe/) is a 2D game making code-centric engine/framework that provides additional higher level functionalities 
on top of [LÖVE](https://www.love2d.org/), making game creation easier, faster, and buzzword! It’s free, open source 
and supported on Windows, Linux and OSX.

# STATUS - DISCONTINUED!!!!!!!!!

You can still access the website http://fuccboi.moe for documentation and this repository for the code, but it will no longer be updated with new changes. Over time I'll release separate modules for each part of the engine that can be reused independently of each other... I think this is a more organized way of doing things and it means other people can use those modules without having to learn a lot of unrelated stuff.

So far the released modules directly from fuccboiGDX are:

* [Input - Thomas](https://github.com/adonaac/thomas)
* [Game UI - Thranduil - WIP](https://github.com/adonaac/thranduil)
* [Text - Popo](https://github.com/adonaac/popo)

The ones I plan on releasing in the future in no particular order:

* Tilemap, takes care of doing tilemap related things -  
* box2d, takes care of box2d object creation/deletion, adds the ability to check for collisions on the update function, ??? - 
* Layer/Render, lets you add/remove objects to layers, apply shaders to an entire layer, to only a subset of objects, etc - 
* Area/Level, lets you add/remove objects to a level, this handles object updating, object queries and so on, ..-
* Particle, game embeddable particle editor + lets you easily load particles into your game (no messing around with files at all)

And the ones built by other people that fuccboiGDX uses:

* [Timer, Camera - hump](http://vrld.github.io/hump/)
* [Animation - AnAL](https://love2d.org/wiki/AnAL)
* [Class - Classic](https://github.com/rxi/classic/)
* [Loader - love-loader](https://github.com/kikito/love-loader)
* [Tool UI - loveframes](http://nikolairesokav.com/projects/loveframes)
* [Sound - TESound](https://love2d.org/wiki/TEsound)
* [Live Coding - lurker](https://github.com/rxi/lurker)
* [Serialization - ser](https://github.com/gvx/Ser)
* [Debug - lovebird](https://github.com/rxi/lovebird)
* [Math - mlib](https://github.com/davisdude/mlib)
* [Functional - moses](https://github.com/Yonaba/Moses)
